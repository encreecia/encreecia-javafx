package org.fealdia.orpg.javafxclient;

import java.net.URL;
import java.util.List;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.util.Duration;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.HeadlessClient;
import org.fealdia.orpg.common.net.data.ChatType;
import org.fealdia.orpg.common.net.data.MusicHint;
import org.fealdia.orpg.common.net.data.ProtoQuest;
import org.fealdia.orpg.javafxclient.controller.Controller;
import org.fealdia.orpg.javafxclient.ui.GenericImageMapper.JavaFxImageMissingListener;
import org.fealdia.orpg.javafxclient.ui.JavaFxImageMapper;

public class JavaFxClient extends HeadlessClient implements JavaFxImageMissingListener {
	private static Logger logger = Logger.getLogger(JavaFxClient.class);

	private final Controller controller;
	private final JavaFxImageMapper imageMapper;

	private boolean musicEnabled = false;
	private MediaPlayer mediaPlayer;
	private MusicHint currentMusicHint;

	public JavaFxClient(Controller controller) {
		this.controller = controller;
		this.imageMapper = new JavaFxImageMapper(this, getFaceMapper());
	}

	@Override
	protected void askLogin() {
		controller.askLogin();
	}

	@Override
	protected void onQuestLogReceived(List<ProtoQuest> quests) {
		controller.onQuestLogReceived(quests);
	}

	@Override
	protected void showMessage(String text) {
		controller.showMessage(text);
	}

	@Override
	public void onBackgroundMissing(int backgroundId) {
		requestBackgroundFaceInfo(backgroundId);
	}

	@Override
	public void onImageMissing(int faceId) {
		super.onImageMissing(faceId);
	}

	@Override
	public void showChat(String who, String text, ChatType chatType) {
		String msg = "[" + chatType + "] <" + who + "> " + text;
		controller.showMessage(msg);
		controller.showChat(msg);
	}

	@Override
	public void setHealth(int health, int maxHealth) {
		super.setHealth(health, maxHealth);
		controller.setHealth(health, maxHealth);
	}

	@Override
	public void setExperience(long experience, long experience_this, long experience_next, int level) {
		super.setExperience(experience, experience_this, experience_next, level);

		controller.setExperience(experience, experience_this, experience_next, level);
	}

	@Override
	protected void onMapChanged() {
		controller.onMapChanged();
	}

	@Override
	protected void onContainerChanged(long id) {
		controller.onContainerChanged(id);
	}

	@Override
	public String getImageFilename(String faceName) {
		return imageMapper.getImageFilename(faceName);
	}

	public JavaFxImageMapper getImageMapper() {
		return imageMapper;
	}

	@Override
	protected void onDisconnect() {
		super.onDisconnect();

		showMessage("Disconnected from server");
	}

	@Override
	protected void offerQuest(int questId, String title, String description) {
		super.offerQuest(questId, title, description);

		controller.offerQuest(questId, title, description);
	}

	@Override
	protected void toggleFullScreen() {
		controller.toggleFullScreen();
	}

	@Override
	protected boolean saveScreenshot(String fullpath) {
		controller.saveScreenshot(fullpath);
		return true;
	}

	@Override
	protected void showPopup(String text) {
		controller.showPopup(text);
	}

	@Override
	protected void onMusicHintReceived(MusicHint musicHint) {
		logger.debug("Music hint: " + musicHint);
		if (currentMusicHint == musicHint) {
			logger.debug("Same as before, ignoring");
			return;
		}
		currentMusicHint = musicHint;
		if (!musicEnabled) {
			return;
		}

		// TODO also vary times of looping depending on type (DEATH only once for example)

		switch (musicHint) {
			case DEATH:
				playMusic(1, false, "death.mp3");
				Animation fadeOut = new Transition() {
					{
						setCycleDuration(Duration.seconds(30));
					}

					@Override
					protected void interpolate(double frac) {
						mediaPlayer.setVolume(1.0 - frac);
					}
				};
				fadeOut.setOnFinished(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						mediaPlayer.stop();
						onMusicHintReceived(MusicHint.GENERAL);
					}
				});
				fadeOut.play();
				mediaPlayer.setOnEndOfMedia(new Runnable() {
					@Override
					public void run() {
						onMusicHintReceived(MusicHint.GENERAL);
					}
				});
				break;
			case TOWN:
				playMusic(MediaPlayer.INDEFINITE, true, "town.mp3");
				break;
			case GENERAL:
			default:
				playMusic(MediaPlayer.INDEFINITE, true, "friends.mp3");
		}
	}

	private void playMusic(final int cycleCount, boolean fadeOut, String filename) {
		URL resource = getClass().getResource("/" + filename);
		if (resource == null) {
			logger.warn("Could not find music: " + filename);
			return;
		}
		final Media media = new Media(resource.toString());

		if (mediaPlayer != null && mediaPlayer.getStatus() == Status.PLAYING) {
			if (!fadeOut) {
				mediaPlayer.stop();
			}
			else {
				Animation transition = new Transition() {
					{
						setCycleDuration(Duration.seconds(10));
					}

					@Override
					protected void interpolate(double frac) {
						mediaPlayer.setVolume(1 - frac);
					}
				};
				transition.setOnFinished(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						mediaPlayer = new MediaPlayer(media);
						mediaPlayer.setCycleCount(cycleCount);
						mediaPlayer.setAutoPlay(true);
						new FadeInTransition(mediaPlayer).play();
					}
				});
				transition.play();
				return;
			}
		}
		mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setCycleCount(cycleCount);
		mediaPlayer.setAutoPlay(true);
	}

	private static class FadeInTransition extends Transition {
		private final MediaPlayer mediaPlayer;

		public FadeInTransition(MediaPlayer mediaPlayer) {
			this.mediaPlayer = mediaPlayer;
			setDelay(Duration.seconds(10));
		}

		@Override
		protected void interpolate(double frac) {
			mediaPlayer.setVolume(frac);
		}

	}

	public void setMusic(boolean on) {
		musicEnabled = on;
	}
}
