package org.fealdia.orpg.javafxclient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;

public class JavaFxBindManager {
	private static Logger logger = Logger.getLogger(JavaFxBindManager.class);

	private Map<KeyCombination,String> combos = new HashMap<KeyCombination,String>();
	private BindListener listener;

	public interface BindListener {
		public void bindPressed(String bind, String command);
	}

	public JavaFxBindManager(BindListener listener) {
		this.listener = listener;

		try {
			loadFromStream(getClass().getResourceAsStream("client.binds"));
		} catch (Exception e) {
			logger.error("Could not load keybindings", e);
		}

		final String filename = PathConfig.getRootDir() + "client.binds";
		try {
			loadFromStream(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
			logger.debug("User keybindings file not found, skipping: " + filename);
		} catch (IOException e) {
			logger.error("Could not load user keybindings", e);
		}
	}

	public void addBind(String combo, String command) {
		logger.debug("addBind " + combo + " -> " + command);
		combos.put(KeyCombination.keyCombination(combo), command);
	}

	public void loadFromStream(InputStream in) throws IOException {
		BufferedReader buf = null;
		try {
			buf = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = buf.readLine()) != null) {
				if (line.startsWith("#") || line.length() == 0) {
					continue;
				}
				String[] parts = line.split(" ", 2);
				addBind(parts[0], parts[1]);
			}
		} finally {
			buf.close();
		}
	}

	public void handle(KeyEvent event) {
		for (KeyCombination combo : combos.keySet()) {
			if (combo.match(event)) {
				final String command = combos.get(combo);
				logger.debug("Keybinding matched: " + combo.toString() + " -> " + command);
				listener.bindPressed(combo.toString(), command);
				event.consume();
				break;
			}
		}
	}
}
