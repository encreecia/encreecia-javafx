package org.fealdia.orpg.javafxclient.controller;

import java.io.IOException;
import java.util.Map;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class AboutController extends AbstractController {
	private static Logger logger = Logger.getLogger(AboutController.class);

	@FXML
	private TextArea authorsTextArea;
	@FXML
	private TextArea licenseTextArea;
	@FXML
	private WebView webView;
	@FXML
	private TextArea debugTextArea;

	public AboutController(final Stage primaryStage) throws IOException {
		super(primaryStage, "fxml/aboutdialog.fxml", "About Encreecia JavaFX client");
		logger.debug("Creating about dialog");

		authorsTextArea.setText(getResourceAsText("/AUTHORS"));
		licenseTextArea.setText(getResourceAsText("/LICENSE"));
		webView.getEngine().load("http://www.gnu.org/licenses/agpl-3.0-standalone.html");
		debugTextArea.setText(getDebugText());
	}

	private String getResourceAsText(String resource) throws IOException {
		return IOUtils.toString(getClass().getResourceAsStream(resource), "utf-8");
	}

	private String getDebugText() {
		StringBuffer res = new StringBuffer();
		for (Map.Entry<Object,Object> e : System.getProperties().entrySet()) {
			res.append(e.getKey() + "=" + e.getValue() + "\n");
		}
		return res.toString();
	}
}
