package org.fealdia.orpg.javafxclient.controller;

import java.io.IOException;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javafx.util.Duration;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.HeadlessClient;

public class QuestOfferController extends AbstractController {
	private static Logger logger = Logger.getLogger(QuestOfferController.class);

	private HeadlessClient client;
	private final int questId;
	@FXML
	private Label questTitle;
	@FXML
	private TextArea questDescription;

	public QuestOfferController(final Stage primaryStage, final HeadlessClient client, int questId, String title, final String description) throws IOException {
		super(primaryStage, "fxml/questoffer.fxml", "Quest offer");
		this.client = client;
		this.questId = questId;

		questTitle.setText(title);

		final Animation animation = new Transition() {
			{
				setCycleDuration(Duration.millis(5000));
			}

			@Override
			protected void interpolate(double frac) {
				final int n = Math.round(description.length() * (float) frac);
				questDescription.setText(description.substring(0, n));
			}
		};
		animation.play();
	}

	public void acceptAction(ActionEvent event) {
		logger.debug("Accept");
		client.sendCommand("acceptquest " + questId);
		hide();
	}

	public void rejectAction(ActionEvent event) {
		logger.debug("Reject");
		client.sendCommand("rejectquest " + questId);
		hide();
	}
}
