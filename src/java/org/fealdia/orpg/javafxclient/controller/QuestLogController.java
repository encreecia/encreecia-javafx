package org.fealdia.orpg.javafxclient.controller;

import java.io.IOException;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.HeadlessClient;
import org.fealdia.orpg.common.net.data.ProtoQuest;

public class QuestLogController extends AbstractController {
	private Logger logger = Logger.getLogger(QuestLogController.class);

	private final List<ProtoQuest> quests;
	private final HeadlessClient client;

	@FXML
	private ListView<String> questList;
	@FXML
	private Label questTitle;
	@FXML
	private TextArea questDescription;

	public QuestLogController(Stage primaryStage, final List<ProtoQuest> quests, HeadlessClient client) throws IOException {
		super(primaryStage, "fxml/questlog.fxml", "Quest log", StageStyle.DECORATED, Modality.NONE);
		this.quests = quests;
		this.client = client;

		for (ProtoQuest quest : quests) {
			questList.getItems().add(quest.title);
		}
		questList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				int index = questList.getSelectionModel().getSelectedIndex();
				ProtoQuest quest = quests.get(index);
				questTitle.setText(quest.title);
				questDescription.setText(quest.description);
			}
		});
	}

	public void abandonAction(ActionEvent event) {
		int index = questList.getSelectionModel().getSelectedIndex();
		ProtoQuest quest = quests.get(index);
		logger.debug("Abandon quest: " + quest.id);
		client.sendCommand("abandonquest " + quest.id);
		hide();
	}

	public void closeAction(ActionEvent event) {
		logger.debug("Close");
		hide();
	}
}
