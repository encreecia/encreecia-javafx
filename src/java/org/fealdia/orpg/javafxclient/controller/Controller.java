package org.fealdia.orpg.javafxclient.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Animation;
import org.fealdia.orpg.common.maps.Animations;
import org.fealdia.orpg.common.net.data.FaceInfo;
import org.fealdia.orpg.common.net.data.ProtoQuest;
import org.fealdia.orpg.common.net.packets.ContainerPacket.ContainerItem;
import org.fealdia.orpg.javafxclient.JavaFxBindManager;
import org.fealdia.orpg.javafxclient.JavaFxBindManager.BindListener;
import org.fealdia.orpg.javafxclient.JavaFxClient;
import org.fealdia.orpg.javafxclient.ui.ContainerItemView;
import org.fealdia.orpg.javafxclient.ui.JavaFxImageMapper;
import org.fealdia.orpg.javafxclient.ui.MinimapCanvas;
import org.fealdia.orpg.javafxclient.ui.PlayViewCanvas;

/**
 * JavaFX Controller class for client.fxml. Handles actions from UI, and contains callbacks for JavaFxClient.
 */
public class Controller implements BindListener {
	@FXML
	private Pane rootPane;
	@FXML
	private TextField commandLine;
	@FXML
	private TextArea generalTextArea;
	@FXML
	private TextArea chatTextArea;
	@FXML
	private Pane canvasPane;
	@FXML
	private StackPane minimapPane;
	@FXML
	private ProgressBar healthProgressBar;
	@FXML
	private Label healthLabel;
	@FXML
	private ProgressBar experienceProgressBar;
	@FXML
	private Label experienceLabel;
	@FXML
	private FlowPane inventoryPane;
	@FXML
	private FlowPane floorPane;

	private final static Logger logger = Logger.getLogger(Controller.class);

	private JavaFxClient client;
	private int movementVectorX = 0;
	private int movementVectorY = 0;
	private MinimapCanvas minimapCanvas;
	private JavaFxImageMapper imageMapper;
	private Stage primaryStage;
	private JavaFxBindManager bindManager = new JavaFxBindManager(this);

	public void initialize() {
		logger.debug("Controller initialize");
	}

	public void addCanvas() {
		final PlayViewCanvas canvas = new PlayViewCanvas(672, 672, client, imageMapper);
		canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				int x = canvas.getScenePositionXFromPixel((int) e.getX());
				int y = canvas.getScenePositionYFromPixel((int) e.getY());
				client.handleClick(x, y, e.isControlDown(), e.getClickCount());
			}
		});
		canvas.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.isShiftDown()) {
					int x = canvas.getScenePositionXFromPixel((int) e.getX());
					int y = canvas.getScenePositionYFromPixel((int) e.getY());
					client.handleShiftHover(x, y);
				}
			}
		});
		canvasPane.getChildren().add(canvas);

		new AnimationTimer() {
			private long lastAnimated = 0;

			@Override
			public void handle(long now) {
				if (now - lastAnimated < 100000000) {
					// Don't draw if last time was less than 0.1 sec ago
					return;
				}
				lastAnimated = now;
				try {
					canvas.draw();
				} catch (Exception e) {
					logger.warn("Exception when drawing PlayViewCanvas", e);
				}
			}
		}.start();
	}

	public void addMinimap() {
		final MinimapCanvas canvas = new MinimapCanvas();
		canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				int x = canvas.getRealX((int) event.getX());
				int y = canvas.getRealY((int) event.getY());
				client.sendMapClicked(x, y);
			}
		});
		minimapPane.getChildren().add(canvas);
		minimapCanvas = canvas;
	}

	public void commandLineAction(ActionEvent event) {
		final String text = commandLine.getText();
		if (text.length() == 0) {
			rootPane.requestFocus();
			return;
		}
		logger.debug("Command line text: " + text);

		if (text.startsWith("/")) {
			client.handleCommand(text.substring(1));
		} else {
			client.handleCommand("say " + text);
		}

		commandLine.clear();
		rootPane.requestFocus();
	}

	public JavaFxClient getClient() {
		return client;
	}

	public void handle(KeyEvent event) {
		if (event.getEventType() == KeyEvent.KEY_PRESSED) {
			switch (event.getCode()) {
				case ENTER:
					if (event.getEventType() == KeyEvent.KEY_PRESSED) {
						commandLine.requestFocus();
					}
					return;
				case ESCAPE:
					rootPane.requestFocus();
					return;
				case LEFT:
				case A:
					movementVectorX = -1;
					break;
				case RIGHT:
				case D:
					movementVectorX = 1;
					break;
				case UP:
				case W:
					movementVectorY = -1;
					break;
				case DOWN:
				case S:
					movementVectorY = 1;
					break;
				default:
					bindManager.handle(event);
					return;
			}
		}
		else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
			switch (event.getCode()) {
				case LEFT:
				case A:
				case RIGHT:
				case D:
					movementVectorX = 0;
					break;
				case UP:
				case W:
				case DOWN:
				case S:
					movementVectorY = 0;
					break;
				default:
					return;
			}
		}
		client.sendMovementVector(movementVectorX, movementVectorY);
		event.consume();
	}

	public void quitAction(ActionEvent event) {
		Platform.exit();
	}

	public void helpKeysAction(ActionEvent event) throws IOException {
		String text = IOUtils.toString(getClass().getResourceAsStream("/CLIENTHELP"), "utf-8");
		new PopupController(primaryStage, "Keys", text);
	}

	public void aboutAction(ActionEvent event) throws IOException {
		new AboutController(primaryStage);
	}

	public void setClient(JavaFxClient client) {
		this.client = client;
	}

	public void showMessage(final String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				generalTextArea.appendText(text + "\n");
			}
		});
	}

	public void showChat(final String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				chatTextArea.appendText(text + "\n");
			}
		});
	}

	public void setHealth(final int health, final int maxHealth) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				healthProgressBar.setProgress((1.0 * health) / maxHealth);
				healthLabel.setText("" + health + " / " + maxHealth);
			}
		});
	}

	public void setExperience(final long experience, final long experience_this, final long experience_next, final int level) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				experienceProgressBar.setProgress((1.0 * experience - experience_this) / (experience_next - experience_this));
				experienceLabel.setText("Experience " + experience + "/" + experience_next + ", Level " + level);
			}
		});
	}

	public void onMapChanged() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				minimapCanvas.setScene(client.getScene());
				minimapCanvas.setSceneShroud(!client.isMaster() ? client.getSceneShroud() : null);
				minimapCanvas.setPlayerPosition(client.getX(), client.getY());
			}
		});
	}

	public void onContainerChanged(final long id) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (id == 0) {
					populateContainer(inventoryPane, id);
				}
				else if (id == -1) {
					populateContainer(floorPane, id);
				}
				else {
					throw new RuntimeException("Container support not yet implemented");
				}
			}
		});
	}

	private void populateContainer(FlowPane pane, long id) {
		List<ContainerItem> containerItems = client.getContainerItems(id);
		pane.getChildren().clear();
		for (final ContainerItem item : containerItems) {
			FaceInfo faceInfo = client.getFaceMapper().getFaceById(item.faceId);
			String faceName;
			Image image;
			ImageView imageView = new ImageView();
			if (faceInfo != null) {
				faceName = faceInfo.name;
				if (faceName.startsWith(Animation.PREFIX)) {
					Animation animation = Animations.getInstance().getAnimation(faceName.substring(Animation.PREFIX.length()));
					new ItemAnimationTimer(imageView, animation, item.id).start(); // TODO what about GC?
					image = imageMapper.getImage(item.faceId);
				}
				else {
					image = imageMapper.getImage(item.faceId);
				}
			}
			else {
				image = imageMapper.getLoadingImage();
			}
			imageView.setImage(image);
			ContainerItemView view = new ContainerItemView(item, imageView);
			view.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent e) {
					logger.debug("Mouse: " + e);
					if (e.getButton() == MouseButton.MIDDLE) {
						client.sendItemApply(item.id);
					}
					else if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
						client.sendExamineItem(item.id);
					}
					else if (e.getButton() == MouseButton.SECONDARY) {
						// TODO ask for quantity if shift down
						client.sendItemGet(item.id, item.quantity);
					}
					e.consume();
				}

			});
			pane.getChildren().add(view);
		}
	}

	private class ItemAnimationTimer extends AnimationTimer {
		private long lastAnimated = 0;
		private ImageView imageView;
		private Animation animation;
		private long seed;

		public ItemAnimationTimer(ImageView imageView, Animation animation, long seed) {
			this.imageView = imageView;
			this.animation = animation;
			this.seed = seed;
		}

		@Override
		public void handle(long nowNanoSecs) {
			long msecs = nowNanoSecs / 100000;
			if (msecs > lastAnimated + 100) {
				lastAnimated = msecs;
				String frameFace = animation.getFrame(System.currentTimeMillis() / 100, seed);
				imageView.setImage(imageMapper.getImage(frameFace));
			}
		}
	}

	public void setImageMapper(JavaFxImageMapper imageMapper) {
		this.imageMapper = imageMapper;
	}

	public void askLogin() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					new LoginController(primaryStage, client);
				} catch (IOException e) {
					logger.error("Exception when trying to show login dialog", e);
				}
			}
		});
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public void offerQuest(final int questId, final String title, final String description) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					new QuestOfferController(primaryStage, client, questId, title, description);
				} catch (IOException e) {
					logger.error("Exception when creating quest offer dialog", e);
				}
			}
		});
	}

	public void toggleFullScreen() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				primaryStage.setFullScreen(!primaryStage.isFullScreen());
			}
		});
	}

	public void onQuestLogReceived(final List<ProtoQuest> quests) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					new QuestLogController(primaryStage, quests, client);
				} catch (IOException e) {
					logger.error("Exception when creating quest log window", e);
				}
			}
		});
	}

	@Override
	public void bindPressed(final String bind, final String command) {
		if (client != null) {
			if (command.startsWith("!")) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						commandLine.setText(command.substring(1));
						commandLine.requestFocus();
						commandLine.end();
					}
				});
			}
			else {
				client.handleCommand(command);
			}
		}
	}

	public void saveScreenshot(final String fullpath) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				WritableImage snapshot = primaryStage.getScene().snapshot(null);
				try {
					ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", new File(fullpath));
				} catch (Exception e) {
					logger.error("Failed to save screenshot", e);
				}
			}
		});
	}

	public void showPopup(final String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					new PopupController(primaryStage, "Server message", text);
				} catch (Exception e) {
					logger.error("Exception when trying to show popup", e);
				}
			}
		});
	}

	@FXML
	private void musicAction(ActionEvent event) {
		CheckMenuItem checkMenuItem = (CheckMenuItem) event.getSource();
		boolean on = checkMenuItem.isSelected();
		logger.debug("Toggling music " + on);
		client.setMusic(on);
	}
}
