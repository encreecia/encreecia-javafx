package org.fealdia.orpg.javafxclient.controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class PopupController extends AbstractController {
	@FXML
	private TextArea textArea;

	public PopupController(Stage primaryStage, String title, String text) throws IOException {
		super(primaryStage, "fxml/popupdialog.fxml", title);
		textArea.setText(text);
	}

	@FXML
	private void closeAction(ActionEvent event) {
		hide();
	}
}
