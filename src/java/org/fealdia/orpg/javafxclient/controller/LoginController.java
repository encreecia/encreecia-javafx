package org.fealdia.orpg.javafxclient.controller;

import java.io.IOException;
import java.util.prefs.BackingStoreException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.HeadlessClient;
import org.fealdia.orpg.client.ui.LoginDetails;

public class LoginController extends AbstractController {
	private static Logger logger = Logger.getLogger(LoginController.class);

	private HeadlessClient client;
	@FXML
	private TextField usernameField;
	@FXML
	private TextField passwordField;
	@FXML
	private CheckBox rememberCheckBox;

	public LoginController(final Stage primaryStage, final HeadlessClient client) throws IOException {
		super(primaryStage, "fxml/logindialog.fxml", "Log in to " + client.getServerName());
		this.client = client;

		LoginDetails loginDetails = client.getPasswordStore().getLoginDetails(client.getServerName());
		if (loginDetails != null) {
			usernameField.setText(loginDetails.getUsername());
			passwordField.setText(loginDetails.getPassword());
			rememberCheckBox.setSelected(true);
		}
	}

	// from logindialog.xml
	public void loginAction(ActionEvent event) throws BackingStoreException {
		logger.debug("Login dialog login");

		String username = usernameField.getText();
		String password = passwordField.getText();
		hide();

		if (username.length() > 0 && password.length() > 0) {
			client.login(username, password);
		}
		if (rememberCheckBox.isSelected()) {
			client.getPasswordStore().storeLoginDetails(client.getServerName(), new LoginDetails(username, password));
		}
		else {
			client.getPasswordStore().clearLoginDetails(client.getServerName());
		}
	}

	public void cancelAction(ActionEvent event) {
		logger.debug("Login dialog cancel");
		hide();
	}
}
