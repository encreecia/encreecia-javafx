package org.fealdia.orpg.javafxclient.controller;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.fealdia.orpg.javafxclient.MainApp;

public abstract class AbstractController {
	protected final Stage primaryStage;
	protected final Stage stage;

	public AbstractController(Stage primaryStage, String fxmlPath, String title) throws IOException {
		this(primaryStage, fxmlPath, title, StageStyle.UTILITY, Modality.WINDOW_MODAL);
	}

	public AbstractController(Stage primaryStage, String fxmlPath, String title, StageStyle stageStyle, Modality modality) throws IOException {
		this.primaryStage = primaryStage;

		FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(fxmlPath));
		loader.setController(this);
		AnchorPane pane = (AnchorPane) loader.load();

		stage = new Stage(stageStyle);
		stage.initOwner(primaryStage);
		stage.initModality(modality);
		stage.setTitle(title);
		stage.setScene(new Scene(pane));
		stage.show();
	}

	public void hide() {
		// Workaround for a Gtk bug https://javafx-jira.kenai.com/browse/RT-30294
		// Calling stage.hide() directly causes JVM segfault for defaultbutton, and NPE for cancelbutton
		// (when triggered via enter/esc respectively)
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				stage.hide();
			}
		});
	}
}
