package org.fealdia.orpg.javafxclient.ui;

import javafx.scene.image.Image;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.net.data.FaceMapper;

public class JavaFxImageMapper extends GenericImageMapper {
	private static final Logger logger = Logger.getLogger(JavaFxImageMapper.class);

	private ImageCache imageCache;

	public JavaFxImageMapper(JavaFxImageMissingListener listener, FaceMapper faceMapper) {
		super(listener, faceMapper);
		imageCache = new ImageCache();
	}

	public Image getBackgroundImage(int backgroundId) {
		logger.trace("getBackgroundImage(" + backgroundId + ")");
		String filename = getBackgroundImageFilename(backgroundId);
		if (filename == null) {
			return getLoadingImage();
		}
		return imageCache.getImage(filename);
	}

	public Image getImage(String faceName) {
		logger.trace("getImage(" + faceName + ")");
		String filename = getImageFilename(faceName);
		return imageCache.getImage(filename);
	}

	public Image getImage(int faceId) {
		logger.trace("getImage(" + faceId + ")");
		String faceName = faceMapper.getFaceById(faceId).name;
		return getImage(faceName);
	}

	public Image getLoadingImage() {
		return imageCache.getLoadingImage();
	}
}
