package org.fealdia.orpg.javafxclient.ui;

import javafx.scene.canvas.Canvas;

import org.fealdia.orpg.javafxclient.JavaFxClient;

public class PlayViewCanvas extends Canvas {
	private CanvasDrawer canvasDrawer;

	public PlayViewCanvas(double width, double height, JavaFxClient client, JavaFxImageMapper imageMapper) {
		super(width, height);
		canvasDrawer = new CanvasDrawer(getGraphicsContext2D(), client, imageMapper);
	}

	public void draw() {
		canvasDrawer.drawCanvas();
	}

	public int getScenePositionXFromPixel(int pixelX) {
		int relativeX = pixelX / 32;

		return relativeX + canvasDrawer.getOffsetX();
	}

	public int getScenePositionYFromPixel(int pixelY) {
		int relativeY = pixelY / 32;
		return relativeY + canvasDrawer.getOffsetY();
	}
}
