package org.fealdia.orpg.javafxclient.ui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import org.fealdia.orpg.client.SceneShroud;
import org.fealdia.orpg.common.maps.Scene;

public class MinimapCanvas extends Canvas {
	private Scene scene;
	private int x = 0;
	private int y = 0;
	private SceneShroud sceneShroud;

	public MinimapCanvas() {
		super(160, 160);
	}

	private void draw() {
		GraphicsContext gc = getGraphicsContext2D();
		gc.setFill(Color.BLACK);
		gc.fillRect(0, 0, getWidth(), getHeight());

		if (scene == null) {
			return;
		}

		for (int i = 0; i < getWidth(); i++) {
			int realX = (int) (i + x - getWidth() / 2);
			if (realX < 0 || realX >= scene.getWidth()) {
				continue;
			}
			for (int j = 0; j < getHeight(); j++) {
				int realY = (int) (j + y - getHeight() / 2);
				if (realY < 0 || realY >= scene.getHeight()) {
					continue;
				}

				java.awt.Color color = scene.getBackgroundTile(realX, realY).getColor();
				if (sceneShroud != null && sceneShroud.isShrouded(realX, realY)) {
					color = java.awt.Color.BLACK;
				}
				gc.setStroke(Color.rgb(color.getRed(), color.getGreen(), color.getBlue()));
				gc.strokeRect(i, j, 1, 1);
			}
		}

		// Player marker
		gc.setStroke(Color.WHITE);
		gc.strokeRect(getWidth() / 2, getHeight() / 2, 1, 1);
	}

	public int getRealX(int pixelX) {
		return (int) (pixelX + x - getWidth() / 2);
	}

	public int getRealY(int pixelY) {
		return (int) (pixelY + y - getHeight() / 2);
	}

	public void setPlayerPosition(int x, int y) {
		this.x = x;
		this.y = y;
		draw();
	}

	public void setScene(Scene scene) {
		this.scene = scene;
		draw();
	}

	public void setSceneShroud(SceneShroud sceneShroud) {
		this.sceneShroud = sceneShroud;
		draw();
	}
}
