package org.fealdia.orpg.javafxclient.ui;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;

import org.apache.log4j.Logger;

public class ImageCache {
	private static Logger logger = Logger.getLogger(ImageCache.class);

	private Map<String,Image> images = new HashMap<String,Image>();
	private Image loading;

	public ImageCache() {
		loading = new Image(getClass().getResourceAsStream("/org/fealdia/orpg/javafxclient/images/loading.png"));
	}

	public Image getImage(String filename) {
		if (!images.containsKey(filename)) {
			if (!new File(filename).exists()) {
				return loading;
			}
			logger.debug("Loading: " + filename);
			images.put(filename, new Image("file://" + filename));
		}
		return images.get(filename);
	}

	public Image getLoadingImage() {
		return loading;
	}
}
