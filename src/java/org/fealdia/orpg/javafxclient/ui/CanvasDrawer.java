package org.fealdia.orpg.javafxclient.ui;

import java.util.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import org.fealdia.orpg.client.ClientEntity;
import org.fealdia.orpg.common.maps.LineOfSight;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.net.data.FaceInfo;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.javafxclient.JavaFxClient;

public class CanvasDrawer {
	private final GraphicsContext gc;
	private final JavaFxClient client;
	private final JavaFxImageMapper imageMapper;
	private final int width;
	private final int height;

	public CanvasDrawer(GraphicsContext gc, JavaFxClient client, JavaFxImageMapper imageMapper) {
		this.gc = gc;
		this.client = client;
		this.imageMapper = imageMapper;
		this.width = (int) gc.getCanvas().getWidth();
		this.height = (int) gc.getCanvas().getHeight();
	}

	public void drawCanvas() {
		drawBackground();
		if (client.isConnected() && client.getScene() != null) {
			synchronized (client.getEntityCache()) {
				drawScene();
				drawEntities();
			}
		}
	}

	public void drawBackground() {
		gc.setFill(Color.GRAY);
		gc.fillRect(0, 0, width, height);
	}

	public void drawScene() {
		final Scene scene = client.getScene();

		// Draw background tiles
		gc.setFill(Color.WHITE);
		for (int i = 0; i < width / 32; i++) {
			final int realx = i + getOffsetX();
			if (realx < 0 || realx >= scene.getWidth()) {
				continue;
			}
			for (int j = 0; j < height / 32; j++) {
				final int realy = j + getOffsetY();
				if (realy < 0 || realy >= scene.getHeight()) {
					continue;
				}

				final int backgroundId = scene.getBackgroundId(realx, realy);
				Image image = imageMapper.getBackgroundImage(backgroundId);

				if (image != null) {
					gc.drawImage(image, i * 32, j * 32);
				} else {
					gc.fillText("" + backgroundId, i * 32, j * 32);
				}
			}
		}
	}

	public void drawEntities() {
		final int widthTiles = (int) (width / 32);
		final int heightTiles = (int) (height / 32);
		final Scene scene = client.getScene();
		final FaceMapper faceMapper = client.getFaceMapper();
		final int tileSize = 32;
		final int tick = 0; // FIXME
		LineOfSight los = new LineOfSight(scene, client.getX(), client.getY());

		Map<Long,ClientEntity> entityCache = client.getEntityCache();

		synchronized (entityCache) {
			for (int i = 0; i < widthTiles; i++) {
				int real_x = i + getOffsetX();

				if (real_x < 0 || real_x >= scene.getWidth()) {
					continue;
				}

				for (int j = 0; j < heightTiles; j++) {
					int real_y = j + getOffsetY();

					if (real_y < 0 || real_y >= scene.getHeight()) {
						continue;
					}

					// Go through entityCache and draw anything in there. This includes the player itself
					for (int layer = 2; layer >= 0; layer--) {
						for (Long id : entityCache.keySet()) {
							if (!client.isEntityComplete(id)) {
								continue;
							}
							ClientEntity info = client.getEntityInfo(id);
							int en_x = info.getX();
							int en_y = info.getY();
							int en_layer = info.getLayer();

							if (en_layer == layer && en_x == real_x && en_y == real_y) {
								// Draw the entity face
								FaceInfo faceInfo = faceMapper.getFaceById(info.getFace());
								if (faceInfo != null && (client.isMaster() || los.isVisible(en_x, en_y))) {
									Image image = imageMapper.getImage(info.getFace());
									gc.drawImage(image, i * tileSize, j * tileSize);
								}

								// If our player character is here, draw a red box in attack mode
								if (id == 0) {
									if (client.isAttackMode()) {
										gc.setFill(Color.RED);
										gc.fillRect(i * tileSize, j * tileSize + tileSize - 3, tileSize - 1, 2);
									}
								}

								// Draw "glowing" box around the entity if it's our target
								if (id != 0 && id == client.getTarget()) {
									final int STEPS = 16;

									// This is 1,2..16,15..1
									int adjust = (int) (tick % STEPS);
									if (tick % (STEPS * 2) >= STEPS) {
										adjust = STEPS - adjust;
									}

									Color c = Color.rgb(127 + (128 / STEPS) * adjust, 0, 0, 1);
									gc.setStroke(c);
									gc.strokeRect(i * tileSize, j * tileSize, tileSize - 1, tileSize - 1);
									//gc.strokeRect((i + 1) * tileSize, (j + 1) * tileSize, tileSize - 3, tileSize - 3);

									// Draw health bar
									int health = info.getHealth();
									if (health > 0) {
										gc.setFill(Color.RED);
										gc.fillRect(i * tileSize, j * tileSize + tileSize - 5, ((tileSize - 1) * health) / 100, 5);
									}
								}

								// Draw any effect on the entity
								if (info.hasEffect()) {
									Image image = imageMapper.getImage(info.getEffectFace());
									gc.drawImage(image, i * tileSize, j * tileSize);
								}

								// Draw any overlay on the entity
								if (info.getOverlay() != null) {
									Image image = imageMapper.getImage(info.getOverlay());
									gc.drawImage(image, i * tileSize, j * tileSize);
								}

								// Draw name on players
								if (info.isPlayer() && info.getName() != null) {
									gc.setFont(Font.font(null, 8));
									java.awt.Color nameColor = info.getNameColor();
									gc.setStroke(Color.rgb(nameColor.getRed(), nameColor.getGreen(), nameColor.getBlue()));
									gc.strokeText(info.getVisibleName(), i * tileSize, j * tileSize, tileSize);
								}
							}
						}
					}

					// if no LoS, shroud
					if (!los.isVisible(real_x, real_y)) {
						if (client.isMaster()) {
							gc.setGlobalAlpha(0.75f);
							gc.setFill(Color.GRAY);
						}
						else {
							gc.setFill(Color.BLACK);
						}
						gc.fillRect(i * tileSize, j * tileSize, tileSize, tileSize);
						gc.setGlobalAlpha(1.0f);
					}
				}
			}
		}
	}

	public int getOffsetX() {
		return client.getX() - (width / 32 - 1) / 2;
	}

	public int getOffsetY() {
		return client.getY() - (height / 32 - 1) / 2;
	}
}
