package org.fealdia.orpg.javafxclient.ui;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

import org.fealdia.orpg.common.net.packets.ContainerPacket.ContainerItem;

public class ContainerItemView extends StackPane {
	public ContainerItemView(ContainerItem item, ImageView imageView) {
		imageView.setPickOnBounds(true); // Transparent area is clickable too
		getChildren().add(imageView);

		// Optional label for quantity
		if (item.quantity > 1) {
			Label quantity = new Label("" + item.quantity);
			setAlignment(quantity, Pos.TOP_LEFT);
			getChildren().add(quantity);
		}

		// Optional label for equipped
		if (item.equipped) {
			Label equipped = new Label("A");
			setAlignment(equipped, Pos.BOTTOM_RIGHT);
			getChildren().add(equipped);
		}
	}
}
