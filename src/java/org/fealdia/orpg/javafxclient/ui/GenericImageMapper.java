package org.fealdia.orpg.javafxclient.ui;

import java.io.File;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Animation;
import org.fealdia.orpg.common.maps.Animations;
import org.fealdia.orpg.common.maps.BackgroundMapper;
import org.fealdia.orpg.common.net.data.FaceInfo;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.StringUtil;

/**
 * Maps numerical backgroundId/faceId to filesystem filename.
 */
public class GenericImageMapper {
	private static Logger logger = Logger.getLogger(GenericImageMapper.class);

	private final JavaFxImageMissingListener listener;
	protected final FaceMapper faceMapper;

	public interface JavaFxImageMissingListener {
		public void onBackgroundMissing(int backgroundId);

		public void onImageMissing(int faceId);
	}

	public GenericImageMapper(JavaFxImageMissingListener listener, FaceMapper faceMapper) {
		this.listener = listener;
		this.faceMapper = faceMapper;
	}

	public String getBackgroundImageFilename(final int backgroundId) {
		// Check faceMapper for background faceName
		final String faceName = BackgroundMapper.getInstance().getTile(backgroundId).getFace();
		if (faceMapper.getFaceByName(faceName) != null) {
			String result = getImageFilename(faceName);
			//logger.debug("Mapping backgroundId " + backgroundId + " to filename " + result);
			return result;
		}

		// Notify ImageMissingListener
		listener.onBackgroundMissing(backgroundId);

		return null;
	}

	public String getImageFilename(String faceName) {
		if (faceName.startsWith(Animation.PREFIX)) {
			final String anim = faceName.substring(Animation.PREFIX.length());
			faceName = Animations.getInstance().getAnimation(anim).getFrameWithSeed(0);
		}

		FaceInfo faceInfo = faceMapper.getFaceByName(faceName);
		final String crc = StringUtil.toHexString(faceInfo.crc32);

		String result = getImageDir() + File.separatorChar + faceName + "_" + crc + ".png";
		// FIXME trashes disk, get rid of this
		if (!new File(result).exists()) {
			listener.onImageMissing(faceInfo.id);
		}
		return result;
	}

	public static String getImageDir() {
		return PathConfig.getImageDir() + "32x32";
	}
}
