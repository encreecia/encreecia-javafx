package org.fealdia.orpg.javafxclient;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.javafxclient.controller.Controller;
import org.fealdia.orpg.javafxclient.ui.JavaFxImageMapper;

/**
 * Main JavaFX application. Loads and creates main Scene. Creates client, image handling classes and event handlers.
 */
public class MainApp extends Application {
	private Logger logger; // Initialize only after log4j.configuration property is set

	private JavaFxClient client;
	private JavaFxImageMapper imageMapper;
	private Controller controller;

	@Override
	public void start(Stage primaryStage) throws IOException {
		parseParams(getParameters().getRaw().toArray(new String[0]));
		logger = Logger.getLogger(MainApp.class);

		primaryStage.setTitle("Encreecia JavaFX client");
		primaryStage.getIcons().add(new Image(MainApp.class.getResourceAsStream("images/icon.png")));

		FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("fxml/client.fxml"));
		logger.debug("Loading fxml");
		Pane pane = (Pane) loader.load();

		logger.debug("Creating Scene");
		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.show();

		controller = (Controller) loader.getController();
		controller.setPrimaryStage(primaryStage);

		scene.addEventHandler(KeyEvent.ANY, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				controller.handle(event);
			}
		});
		pane.requestFocus();

		client = new JavaFxClient(controller);
		controller.setClient(client);
		imageMapper = client.getImageMapper();
		controller.setImageMapper(imageMapper);

		controller.addCanvas();
		controller.addMinimap();

		Thread thread = new Thread(client);
		thread.setDaemon(true);
		thread.start();
		client.handleCommand("connect " + System.getProperty("jfealdia.defaultserver"));
	}

	private void parseParams(final String[] args) {
		PathConfig.initUserRootDir();
		System.setProperty("jfealdia.defaultserver", "localhost:7080");

		for (int i = 0; i < args.length; i++) {
			if ("-defaultserver".equals(args[i])) {
				System.setProperty("jfealdia.defaultserver", args[i + 1]);
				i++;
			}
			else if ("-easyplay".equals(args[i])) {
				System.setProperty("jfealdia.easyplay", "1");
			}
			else if ("-webstart".equals(args[i])) {
				System.setProperty("log4j.configuration", "log4j-client.properties");
			}
			else if ("-wipecache".equals(args[i])) {
				PathConfig.wipeUserCache();
			}
		}
	}

	// main() not called when running from jar/webstart
	public static void main(String[] args) {
		launch(args);
	}
}
